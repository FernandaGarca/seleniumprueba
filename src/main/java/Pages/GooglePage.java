package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Com.BasePage;

public class GooglePage extends BasePage {
		
	public GooglePage(WebDriver driver) {
		super(driver);
	}

	By searchField = By.xpath("//*[@id='tsf']/div[2]/div[1]/div[1]/div/div[2]/input");
	By logIn = By.linkText("Iniciar sesi�n");
	

	public void SearchField() {
		type(searchField, "gmail.com");
		driver.findElement(logIn).click();
	}
	
	
}
