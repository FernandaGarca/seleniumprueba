package Com;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTest {
	protected WebDriver driver;
	String ChromeD ="C:\\ChromeDriver\\chromedriver\\chromedriver.exe";
	
		
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", ChromeD);
		driver = new ChromeDriver();
		
	}
	
	public void exit() {
		driver.quit();
	}
}
