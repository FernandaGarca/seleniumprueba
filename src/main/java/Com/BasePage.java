package Com;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class BasePage {

	protected WebDriver driver;
	
	public BasePage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebDriver chromeDriverConnectio() {
		System.setProperty("webdriver.chrome.driver", "C:\\ChromeDriver\\chromedriver\\chromedriver.exe");
		driver = new ChromeDriver();
		return driver;
	}
	
	public void urlPage(String url) {
		driver.get(url);
	}
	
	public String getText(WebElement element) {
		return element.getText();
	}
	
	public String getText(By locator) {
		return driver.findElement(locator).getText();
		
	}
	
	public void type(By locator, String text) {
		driver.findElement(locator).sendKeys(text);
	}
	
	public void ClickOnLocator(By locator) {
		driver.findElement(locator).click();		
	}
	

	public Boolean isDisplayed(By locator) {
		try {
			return driver.findElement(locator).isDisplayed();
		} catch (org.openqa.selenium.NoSuchElementException e) {
			return false;
		}
	}
		
	public void WaitToBeclickable(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(locator));
	}
	
	
}
